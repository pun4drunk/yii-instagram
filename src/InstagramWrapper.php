<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ECodebird
 *
 * @author Vladislav
 */
require_once Yii::getPathOfAlias('cinstagram').'/Instagram.php';

use MetzWeb\Instagram\Instagram;

class InstagramWrapper extends Instagram {
    
    const CATEGORY_ALL      = 'all';
    const CATEGORY_FRIENDS  = 'relationship';
    const CATEGORY_LIKES    = 'likes';
    const CATEGORY_COMMENTS    = 'comments';
    
    const METHOD_POST = 'POST';
    
    const ACTION_NONE     = 'none';
    const ACTION_FOLLOW   = 'follow';
    const ACTION_UNFOLLOW = 'unfollow';
    
    /**
    * Whether a signed header should be used
    *
    * @var boolean
    */
    private $_signedheader = false;
    
    /**
    * The user access token
    *
    * @var string
    */
    private $_accesstoken;
    
    /**
    * The Instagram OAuth API secret
    *
    * @var string
    */
    private $_apisecret;

  
    public $signedHeaderIp;
  
    
    /**
    * API Secret Setter
    *
    * @param string $apiSecret 
    * @return void
    */
    public function setApiSecret($apiSecret) {
      $this->_apisecret = $apiSecret;
    }
  
    public function getRequestCategory($function, $method) {
        
        $category = self::CATEGORY_ALL;
        
        if (self::METHOD_POST === $method) {
            $category = basename($function);
        }
        
        return $category;
        
    }
    
    public function _makeCall($function, $auth = false, $params = null, $method = 'GET') {//proxy
        if (false === $auth) {
            // if the call doesn't requires authentication
            $authMethod = '?client_id=' . $this->getApiKey();
        } else {
                // if the call needs an authenticated user
            if (true === isset($this->_accesstoken)) {
                  $authMethod = '?access_token=' . $this->getAccessToken();
            } else {
                  throw new \Exception("Error: _makeCall() | $function - This method requires an authenticated users access token.");
            }
        }

        if (isset($params) && is_array($params)) {
            $paramString = '&' . http_build_query($params);
        } else {
            $paramString = null;
        }

        $apiCall = self::API_URL . $function . $authMethod . (('GET' === $method) ? $paramString : null);
        
        return array($function, $auth, $params, $method, $apiCall, $paramString);
    }
    
    public function _makeApiCall($apiCall, $params, $paramString, $method) {
        
        // signed header of POST/DELETE requests
        $headerData = array('Accept: application/json');
        if (true === $this->_signedheader && 'GET' !== $method) {
          $headerData[] = 'X-Insta-Forwarded-For: ' . $this->_signHeader();
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiCall);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerData);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        curl_setopt($ch, CURLOPT_HEADER, 1);

        if ('POST' === $method) {
          curl_setopt($ch, CURLOPT_POST, count($params));
          curl_setopt($ch, CURLOPT_POSTFIELDS, ltrim($paramString, '&'));
        } else if ('DELETE' === $method) {
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        $jsonData = curl_exec($ch);
        list($jsonData, $headers) = $this->_parseApiReply(curl_exec($ch));

        if (false === $jsonData) {
          throw new \Exception("Error: _makeCall() - cURL error: " . curl_error($ch));
        }
        
        
        $result = json_decode($jsonData);
        
        if (is_object($result)) {
            $result->rate = $this->_getRateLimitInfo($headers);
            $result->httpstatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        }
        
        curl_close($ch);
        
        return $result;
    }
    
    protected function _parseApiReply($reply) {
        // split headers and body
        $headers = array();
        $reply = explode("\r\n\r\n", $reply, 4);

        // check if using proxy
        $proxy_strings = array();
        $proxy_strings[strtolower('HTTP/1.0 200 Connection Established')] = true;
        $proxy_strings[strtolower('HTTP/1.1 200 Connection Established')] = true;
        if (array_key_exists(strtolower(substr($reply[0], 0, 35)), $proxy_strings)) {
            array_shift($reply);
        } elseif (count($reply) > 2) {
            $headers = array_shift($reply);
            $reply = array(
                $headers,
                implode("\r\n", $reply)
            );
        }

        $headers_array = explode("\r\n", $reply[0]);
        foreach ($headers_array as $header) {
            $header_array = explode(': ', $header, 2);
            $key = $header_array[0];
            $value = '';
            if (count($header_array) > 1) {
                $value = $header_array[1];
            }
            $headers[$key] = $value;
        }
        
        if (count($reply) > 1) {
            $body = $reply[1];
        } else {
            $body = '';
        }
        
        return array($body, $headers);
        
    }
    
    protected function _getRateLimitInfo($headers)
    {
        
        if (! isset($headers['X-Ratelimit-Limit'])) {
            return null;
        }
        $result = array(
            'limit'     => $headers['X-Ratelimit-Limit'],
            'remaining' => $headers['X-Ratelimit-Remaining'],
        );
        
        return $result;
    }
    
    /**
   * Access Token Setter
   *
   * @param object|string $data
   * @return void
   */
  public function setAccessToken($data) {
    (true === is_object($data)) ? $token = $data->access_token : $token = $data;
    $this->_accesstoken = $token;
  }

  /**
   * Access Token Getter
   *
   * @return string
   */
  public function getAccessToken() {
    return $this->_accesstoken;
  }
  
  /**
   * API Secret Getter
   *
   * @return string
   */
  public function getApiSecret() {
    return $this->_apisecret;
  }
  
  
  
  /**
   * Pagination feature
   *
   * @param object  $obj                  Instagram object returned by a method
   * @param integer $limit                Limit of returned results
   * @return mixed
   */
  public function pagination($obj, $limit = 0) {
    if (true === is_object($obj) && !is_null($obj->pagination)) {
      if (!isset($obj->pagination->next_url)) {
        return;
      }
      $apiCall = explode('?', $obj->pagination->next_url);
      if (count($apiCall) < 2) {
        return;
      }
      $function = str_replace(self::API_URL, '', $apiCall[0]);
      $auth = (strpos($apiCall[1], 'access_token') !== false);
      if (isset($obj->pagination->next_max_id)) {
        return $this->_makeCall($function, $auth, array('max_id' => $obj->pagination->next_max_id, 'count' => $limit));
      } else if (isset($obj->pagination->next_max_like_id)){
        return $this->_makeCall($function, $auth, array('max_like_id' => $obj->pagination->next_max_like_id, 'count' => $limit));
      } else {
        return $this->_makeCall($function, $auth, array('cursor' => $obj->pagination->next_cursor, 'count' => $limit));
      }
    } else {
      throw new \Exception("Error: pagination() | This method doesn't support pagination.");
    }
  }
  
    /**
     * Enforce Signed Header
     *
     * @param boolean $signedHeader
     * @return void
     */
    public function setSignedHeader($signedHeader) {
      $this->_signedheader = $signedHeader;
    }
  
    /**
     * Sign header by using the app's IP and its API secret
     *
     * @return string                       The signed header
     */
    private function _signHeader() {
        $ipAddress = $this->signedHeaderIp ? $this->signedHeaderIp : $_SERVER['SERVER_ADDR'];
        $signature = hash_hmac('sha256', $ipAddress, $this->_apisecret, false);
        $header = join('|', array($ipAddress, $signature));
        return $header;
    }

}
