<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ECodebird
 *
 * @author Vladislav
 */

Yii::import('einstagram.InstagramWrapper');

class EInstagram extends YiiApi\components\Api {
    
    const MEDIA_TYPE_IMAGE = 'image';
    const MEDIA_TYPE_VIDEO = 'video';
    
    const OAUTH_PARAMETER_EXCEPTION = 'OAuthParameterException';
    
    public $name = 'api.instagram';
    public $clientName = 'instagram';
    protected $_initialized=false;
    
    protected $rawActions = array(
        'getLoginUrl',
        'setAccessToken',
        'getAccessToken',
        'setApiKey',
        'getApiKey',
        'setApiSecret',
        'getApiSecret',
        'setApiCallback',
        'getApiCallback',
        'setSignedHeader',
        'getOAuthToken',
    );
    
    protected $requestAuth;
    protected $requestParamString;
    
    public $requestRates = array();
    
    public $videoQuality = 'standard_resolution';
    protected $defaultVideoQuality  = 'standard_resolution';
    
    public $imageQuality = 'standard_resolution';
    protected $defaultImageQuality  = 'standard_resolution';
    
    public $signedHeaderIp = false;
    
    protected function internalBehaviors() {
        return \CMap::mergeArray(parent::internalBehaviors(), array(
            'apiRates' => array(
                'class' => 'YiiApi\behaviors\ApiRatesBehavior',
                'floating' => true,
                'rates' => \CMap::mergeArray(array(
                    InstagramWrapper::CATEGORY_ALL => array(
                        'limit' => 5000,
                    ),
                    InstagramWrapper::CATEGORY_LIKES => array(
                        'limit' => 100,
                    ),
                    InstagramWrapper::CATEGORY_FRIENDS => array(
                        'limit' => 60,
                    ),
                    InstagramWrapper::CATEGORY_COMMENTS => array(
                        'limit' => 60,
                    ),
                ), $this->requestRates),
                'interval' => 3600,
            ),
        ));
    }
    
    public function isRawAction($name) {
        return in_array($name, $this->rawActions);
    }
    
    public function getApi() {
        
        $this->_api = new InstagramWrapper($this->clientConfig);
        $this->_api->signedHeaderIp = $this->signedHeaderIp;
        $this->_api->setSignedHeader(true);
        
        $this->_initialized = true;
        
    }
    
    protected function authorizeUser() {
        $this->isAuthorized = false;
        if ($this->clientId = $this->user->getClientId($this->clientName)) { 
            if ($token = $this->user->getToken($this->clientName)) {
                $this->_api->setAccessToken($token);
                $this->isAuthorized = true;
            }    
        }
        return $this->isAuthorized;
    }
    
    public static function getText($entry) {
        
        $text = "";
        if (property_exists($entry, 'caption')) {
            if (is_object($entry->caption) 
                    && property_exists($entry->caption, 'text')) {
                $text = $entry->caption->text;
            }
        }
        
        return $text;
    }
    
    public function beforeApiRequest() {
        
        list(
            $name, 
            $this->requestAuth, 
            $params, 
            $method,
            $url,
            $this->requestParamString) = call_user_func_array(array($this->_api, $this->apiRequest->get('name')), $this->apiRequest->get('params'));

        $this->apiRequest->setAttributes(array(
            'name' => $name, 
            'params' => $params,
            'method' => $method,
            'url' => $url,
            'category' => $this->_api->getRequestCategory($name, $method),
        ));
        
        return parent::beforeApiRequest();
    }
    
    protected function doApiRequest() {
        return $this->_api->_makeApiCall(
                $this->apiRequest->get('url'),
                $this->apiRequest->get('params'),
                $this->requestParamString,
                $this->apiRequest->get('method')
        );
    }
    
    
    public function checkRequestHttpStatus() {
        
        $status = $this->apiRequest->get('status');
        $result = $this->apiRequest->get('result');
        
        switch ($status) {
            
            case self::HTTP_STATUS_TOOMANYREQUESTS:
                $category = $this->apiRequest->get('category');
                $this->apiRates->setOverload($category, time());
                throw new \CException("api reported request rate overload for $category", $status);
                break;
            
            case self::HTTP_STATUS_BADREQUEST:
                if (self::OAUTH_PARAMETER_EXCEPTION === $result->meta->error_type) {
                    $this->user->unsetToken($this->clientName);
                }
                
                throw new ApiFatalException($result->meta->error_message, $status);
                break;
            
            default:
                parent::checkRequestHttpStatus();
                break;
        }
    }
    
    public function getMediaUrl($entry) {
        $url = NULL;
        
        switch ($entry->type) {
            case self::MEDIA_TYPE_IMAGE:
                $imageQuality = $this->imageQuality;
                if (!property_exists($entry->images, $imageQuality)) {
                    $imageQuality = $this->defaultImageQuality;
                }
                $url = $entry->images->{$imageQuality}->url;
                break;
            case self::MEDIA_TYPE_VIDEO:
                $videoQuality = $this->defaultVideoQuality;
                if (!property_exists($entry->videos, $videoQuality)) {
                    $videoQuality = $this->defaultVideoQuality;
                }
                $url = $entry->videos->{$videoQuality}->url;
                break;
            default:
                throw new Exception("Unknown media type: $entry->type", 500);
                break;
        }
        
        return $url;
    }
    
    public static function hasPages($reply) {
        return property_exists($reply, 'pagination') && property_exists($reply->pagination, 'next_url');
    }

}
